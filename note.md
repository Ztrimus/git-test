# git tutorial
- git setup on local machine
```sh
git config --global core.editor "code --wait"
```
- generally use commands
```sh
git init
git status
git add .
git commit -m "commit message"
git log
git log --oneline
git checkout [previous commit number] [file which you want to check out]
git reset HEAD [file which you want to checked out]
git checkout -- [file which you want to revert check out back]
```
- For online repo like Github or Bitbucket
```sh
git remote add origin <repo URL>
git push -u origin master
git clone https://Ztrimus@bitbucket.org/Ztrimus/git-test.git
```

# Nodejs and NPM
- install Nodejs and NPM
- After that How to create/Initialize package.json file for project
- enter following line in desired project
```sh
npm init
```
- It'll take you through certain step to setup package.json

### install node module using NPM - Lite Server
```sh
npm install lite-server --save-dev
```
- add following to package.json
```json
"scripts": {
    "start": "npm run lite",
    "lite": "lite-server"
  },
```

- now you can start project by 
```sh
npm start
```
- if you have aleardy package.json file
```sh
npm install
```
to install packages from package.json

### ignore unnecessary files
- create file name `.gitignore`
- add `node_modules` in file